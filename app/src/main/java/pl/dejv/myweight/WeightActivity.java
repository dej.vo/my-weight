package pl.dejv.myweight;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.sheets.v4.SheetsScopes;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.dejv.myweight.core.utils.MyUtils;
import pl.dejv.myweight.core.utils.MyWeightCommonInterface;
import pl.dejv.myweight.core.view.AbstractActivity;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by dejv on 31/08/2017.
 */

public class WeightActivity extends AbstractActivity implements MyWeightView, EasyPermissions.PermissionCallbacks {

	private GoogleAccountCredential mAccountCredential;

	private final String[] SCOPES = {SheetsScopes.SPREADSHEETS};

	@BindView(R.id.weight_input)
	TextView mSimpleText;

	@BindView(R.id.get_my_weight)
	Button mGetWeightBtn;

	@BindView(R.id.my_weight)
	EditText mMyWeight;

	@BindView(R.id.post_my_weight)
	Button mPostWeight;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ButterKnife.bind(this);
		initAccountCredential();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void injectDependencies() {
		mActivityComponent.inject(this);
	}

	@OnClick(R.id.get_my_weight)
	public void onGetWeightClick() {
		getWeightFromApi();
	}

	@OnClick(R.id.post_my_weight)
	public void onPostWeightClick() {
	}

	private void initAccountCredential() {
		mAccountCredential = GoogleAccountCredential.usingOAuth2(
				getApplicationContext(), Arrays.asList(SCOPES))
				.setBackOff(new ExponentialBackOff());
	}

	private void getWeightFromApi() {
		if (!MyUtils.isGooglePlayServicesAvailable(this)) {
			MyUtils.acquireGooglePlayServices(this);
		} else if (mAccountCredential.getSelectedAccountName() == null) {
			chooseGoogleAccount();
		} else if (!MyUtils.isNetworkConnection(this)) {
			mSimpleText.setText("No internet connection!");
		} else {
			// request for weight from API
			mSimpleText.setText("Requesting your weight from Google Sheet API...");
		}
	}

	private void chooseGoogleAccount() {
		if (EasyPermissions.hasPermissions(this, Manifest.permission.GET_ACCOUNTS)) {
			if (mAccountCredential.getSelectedAccountName() == null) {
				startActivityForResult(
						mAccountCredential.newChooseAccountIntent(),
						MyWeightCommonInterface.REQUEST_GOOGLE_ACCOUNT_PICKER);
			} else {
				getWeightFromApi();
			}
		} else {
			EasyPermissions.requestPermissions(this,
					"This app needs to access your Google account",
					MyWeightCommonInterface.REQUEST_PERMISSION_GET_ACCOUNT,
					Manifest.permission.GET_ACCOUNTS);
		}
	}

	@Override
	public void onPermissionsGranted(int requestCode, List<String> perms) {
		switch (requestCode) {
			case MyWeightCommonInterface.REQUEST_PERMISSION_GET_ACCOUNT:
				getWeightFromApi();
				break;
			case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
				getWeightFromApi();
				break;
		}
	}

	@Override
	public void onPermissionsDenied(int requestCode, List<String> perms) {
		switch (requestCode) {
			case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
				mSimpleText.setText("This app will not work properly unless you grant account permission.");
				break;
			case MyWeightCommonInterface.REQUEST_PERMISSION_GET_ACCOUNT:
				mSimpleText.setText("This app needs account permission to work properly.");
				break;
		}
		if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
			new AppSettingsDialog.Builder(this,
					"Your checked `Never ask again` for permission that is important for this app to work properly.")
					.setNegativeButton("Cancel",
							(dialog, which) ->
									mSimpleText.setText("This app will not work properly unless you grant account permission."))
					.build()
					.show();
		}
	}

	@Override
	public void onComplete() {

	}

	@Override
	public void onError(String message) {

	}
}
