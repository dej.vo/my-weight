package pl.dejv.myweight.core.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by dejv on 29/09/2017.
 */

public class HandleError {
    public static void showMsg(final Throwable error, final View view) {
        Snackbar.make(view, error.getMessage(), Snackbar.LENGTH_LONG).show();
    }
}
