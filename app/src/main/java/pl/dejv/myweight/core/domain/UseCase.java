package pl.dejv.myweight.core.domain;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;

/**
 * Created by dejv on 05/02/2017.
 */

public abstract class UseCase<T, Param> implements Interactor<T, Param> {

	private final ThreadExecutor threadExecutor;
	private final PostThreadExecutor postThreadExecutor;
	private CompositeDisposable compositeDisposable;

	public UseCase(ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor) {
		this.threadExecutor = threadExecutor;
		this.postThreadExecutor = postThreadExecutor;
		compositeDisposable = new CompositeDisposable();
	}

	public abstract Observable<T> createObservable(Param param) throws IOException;

	@Override
	public void execute(DisposableObserver<T> observer, Param param) throws IOException {
		Observable<T> observable = createObservable(param)
				.subscribeOn(Schedulers.from(threadExecutor))
				.observeOn(postThreadExecutor.getScheduler());
		compositeDisposable.add(observable.subscribeWith(observer));
	}

	@Override
	public Observable<T> execute(Param param) throws IOException {
		return createObservable(param);
	}

	public void dispose() {
		if (!compositeDisposable.isDisposed()) {
			compositeDisposable.dispose();
		}
	}
}
