package pl.dejv.myweight.core.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.api.services.sheets.v4.model.Spreadsheet;

import pl.dejv.myweight.R;

/**
 * Created by Dawid Krężel on 12/02/2017.
 */

public class MyUtils {

	public static boolean isGooglePlayServicesAvailable(final Context context) {
		GoogleApiAvailability api = GoogleApiAvailability.getInstance();
		final int statusCode = api.isGooglePlayServicesAvailable(context);
		return statusCode == ConnectionResult.SUCCESS;
	}

	public static boolean isNetworkConnection(final Context context) {
		ConnectivityManager connMgr = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}

	public static void acquireGooglePlayServices(final AppCompatActivity activity) {
		GoogleApiAvailability api = GoogleApiAvailability.getInstance();
		final int statusCode = api.isGooglePlayServicesAvailable(activity);
		if (api.isUserResolvableError(statusCode))
			showGooglePlayServicesAvailabilityErrorDialog(statusCode, activity);
	}

	private static void showGooglePlayServicesAvailabilityErrorDialog(final int statusCode, final AppCompatActivity activity) {
		GoogleApiAvailability api = GoogleApiAvailability.getInstance();
		api.getErrorDialog(
				activity,
				statusCode,
				MyWeightCommonInterface.REQUEST_GOOGLE_PLAY_SERVICES)
				.show();
	}

	@Nullable
	public static String getSelectedAccountName(final Context context) {
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getString(context.getString(R.string.account_name_key), null);
	}

	public static void saveAccountName(final Context context, final String accountName) {
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		prefs.edit()
				.putString(context.getString(R.string.account_name_key), accountName)
				.apply();
	}

	public static void saveSpreadsheetId(final Context context, final Spreadsheet sheet) {
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		prefs.edit()
				.putString(context.getString(R.string.spread_sheet_id_key), sheet.getSpreadsheetId())
				.apply();
	}

	@Nullable
	public static String getSpreadsheetId(final Context context) {
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getString(context.getString(R.string.spread_sheet_id_key), null);
	}

	public static void notImplemented(final View view) {
		Snackbar.make(view, "Not implemented", Snackbar.LENGTH_LONG).show();
	}
}
