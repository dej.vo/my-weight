package pl.dejv.myweight.core.utils.module;

import android.app.Activity;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.core.utils.annotation.PerActivity;

/**
 * Created by dejv on 05/02/2017.
 */
@Module
public class ActivityModule {

	private final Activity activity;

	public ActivityModule(Activity activity) {
		this.activity = activity;
	}

	@PerActivity
	@Provides
	Activity provideActivity() {
		return activity;
	}
}
