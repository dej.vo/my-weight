package pl.dejv.myweight.core.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import pl.dejv.myweight.core.utils.MyWeightApp;
import pl.dejv.myweight.core.utils.component.ActivityComponent;
import pl.dejv.myweight.core.utils.component.ApplicationComponent;
import pl.dejv.myweight.core.utils.component.DaggerActivityComponent;
import pl.dejv.myweight.core.utils.module.ActivityModule;

/**
 * Created by Dawid Krężel on 12/02/2017.
 */
public abstract class AbstractActivity extends AppCompatActivity {

	protected ApplicationComponent mAppComponent;
	protected ActivityComponent mActivityComponent;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		injectComponents();
		injectDependencies();
	}

	private void injectComponents() {
		mAppComponent = MyWeightApp.getInstance().getApplicationComponent();
		mActivityComponent = DaggerActivityComponent.builder()
				.applicationComponent(mAppComponent)
				.activityModule(new ActivityModule(this))
				.build();
	}

	protected abstract void injectDependencies();

}
