package pl.dejv.myweight.core.utils.executor;

import io.reactivex.Scheduler;

/**
 * Created by dejv on 05/02/2017.
 */
public interface PostThreadExecutor {
	Scheduler getScheduler();
}
