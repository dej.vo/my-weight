package pl.dejv.myweight.core.domain;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by dejv on 05/02/2017.
 */

public interface Interactor<T, Param> {

	void execute(DisposableObserver<T> observer, Param param) throws IOException;

	Observable<T> execute(Param param) throws IOException;
}
