package pl.dejv.myweight.core.utils.component;

import android.app.Activity;

import dagger.Component;
import pl.dejv.myweight.account.view.SettingsActivity;
import pl.dejv.myweight.core.utils.annotation.PerActivity;
import pl.dejv.myweight.core.utils.module.ActivityModule;
import pl.dejv.myweight.core.utils.module.PresentationModule;
import pl.dejv.myweight.core.view.AbstractActivity;
import pl.dejv.myweight.createSheet.view.CreateSheetActivity;
import pl.dejv.myweight.createSheet.view.CreateSheetFragment;

/**
 * Created by dejv on 05/02/2017.
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = {ActivityModule.class, PresentationModule.class})
public interface ActivityComponent {

	Activity getActivity();

	void inject(final CreateSheetActivity activity);

	void inject(final AbstractActivity activity);

	void inject(final CreateSheetFragment fragment);

	void inject(final SettingsActivity activity);
}
