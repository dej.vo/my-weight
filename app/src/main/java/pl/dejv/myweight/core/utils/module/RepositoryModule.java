package pl.dejv.myweight.core.utils.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.account.data.AccountRepositoryImpl;
import pl.dejv.myweight.account.data.IAccountDataSource;
import pl.dejv.myweight.account.data.IAccountRepository;
import pl.dejv.myweight.createSheet.data.ISheetRepository;
import pl.dejv.myweight.createSheet.data.SheetDataSource;
import pl.dejv.myweight.createSheet.data.SheetRepositoryImpl;
import pl.dejv.myweight.weight.data.IWeightRepository;
import pl.dejv.myweight.weight.data.WeightDataSource;
import pl.dejv.myweight.weight.data.WeightRepositoryImpl;

/**
 * Created by Dawid Krężel on 07/02/2017.
 */
@Module
public class RepositoryModule {

	@Singleton
	@Provides
	IWeightRepository provideWeighRepository(WeightDataSource network) {
		return new WeightRepositoryImpl(network);
	}

	@Singleton
	@Provides
	ISheetRepository provideCreateSheetRepository(SheetDataSource network) {
		return new SheetRepositoryImpl(network);
	}

	@Singleton
	@Provides
	IAccountRepository provideAccountRepository(IAccountDataSource source) {
		return new AccountRepositoryImpl(source);
	}
}
