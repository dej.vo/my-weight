package pl.dejv.myweight.core.utils.module;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.sheets.v4.Sheets;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.core.data.IGoogleSheetClient;

/**
 * Created by Dawid Krężel on 14/02/2017.
 */
@Module
public class APIModule {

	@Singleton
	@Provides
	Sheets provideSheetService(HttpTransport http, JsonFactory jsonFactory, IGoogleSheetClient client) {
		return client.createService(http, jsonFactory);
	}
}
