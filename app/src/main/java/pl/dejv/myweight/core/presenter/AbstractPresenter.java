package pl.dejv.myweight.core.presenter;

import android.content.Context;
import android.support.v4.app.Fragment;

/**
 * Created by dejv on 12/02/2017.
 */
public abstract class AbstractPresenter<T> {

	protected T view;
	protected Context context;

	public void bind(T view) {
		this.view = view;
		if (view instanceof Context) {
			this.context = (Context) view;
		} else if (view instanceof Fragment) {
			context = ((Fragment) view).getActivity();
		}
		onBind();
	}

	public void unbind() {
		view = null;
		context = null;
		onUnbind();
	}

	protected abstract void onBind();

	protected abstract void onUnbind();

}
