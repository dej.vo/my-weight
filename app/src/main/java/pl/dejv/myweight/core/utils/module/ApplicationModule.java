package pl.dejv.myweight.core.utils.module;

import android.content.Context;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.core.data.GoogleSheetClient;
import pl.dejv.myweight.core.data.IGoogleSheetClient;
import pl.dejv.myweight.core.utils.MyWeightApp;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;
import pl.dejv.myweight.core.utils.executor.impl.UIThread;
import pl.dejv.myweight.core.utils.executor.impl.WorkerExecutor;

/**
 * Created by dejv on 05/02/2017.
 */
@Module
public class ApplicationModule {

	private final MyWeightApp myWeightApp;

	public ApplicationModule(MyWeightApp app) {
		myWeightApp = app;
	}

	@Singleton
	@Provides
	Context provideApplicationContext() {
		return myWeightApp;
	}

	@Singleton
	@Provides
	ThreadExecutor provideThreadExecutor(WorkerExecutor workerExecutor) {
		return workerExecutor;
	}

	@Singleton
	@Provides
	PostThreadExecutor providePostThreadExecutor(UIThread uiThread) {
		return uiThread;
	}

	@Singleton
	@Provides
	HttpTransport provideHttpTransport() {
		return AndroidHttp.newCompatibleTransport();
	}

	@Singleton
	@Provides
	JsonFactory provideJsonFactory() {
		return JacksonFactory.getDefaultInstance();
	}

	@Singleton
	@Provides
	IGoogleSheetClient provideGoogleAccountCredential(Context context) {
		return new GoogleSheetClient(context);
	}

}
