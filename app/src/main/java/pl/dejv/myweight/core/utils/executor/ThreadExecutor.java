package pl.dejv.myweight.core.utils.executor;

import java.util.concurrent.Executor;

/**
 * Created by dejv on 05/02/2017.
 */
public interface ThreadExecutor extends Executor {
}
