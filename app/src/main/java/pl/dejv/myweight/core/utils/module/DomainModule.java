package pl.dejv.myweight.core.utils.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.account.data.IAccountRepository;
import pl.dejv.myweight.account.domain.SetAccountUseCase;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;
import pl.dejv.myweight.createSheet.data.ISheetRepository;
import pl.dejv.myweight.createSheet.domain.CreateSheetUseCase;
import pl.dejv.myweight.createSheet.domain.GetCredentialsUseCase;
import pl.dejv.myweight.weight.data.IWeightRepository;
import pl.dejv.myweight.weight.domain.GetBatchWeightDataUseCase;
import pl.dejv.myweight.weight.domain.PostWeightUseCase;

/**
 * Created by Dawid Krężel on 12/02/2017.
 */
@Module
public class DomainModule {

	@Singleton
	@Provides
	PostWeightUseCase providesPostWeightUseCase(final ThreadExecutor executor, final PostThreadExecutor postExecutor, final IWeightRepository repo) {
		return new PostWeightUseCase(executor, postExecutor, repo);
	}

	@Singleton
	@Provides
	CreateSheetUseCase providesCreateSheetUseCase(final ThreadExecutor executor, final PostThreadExecutor postExecutor, final ISheetRepository repository) {
		return new CreateSheetUseCase(executor, postExecutor, repository);
	}

	@Singleton
	@Provides
	SetAccountUseCase providesSetAccountUseCase(final ThreadExecutor executor, final PostThreadExecutor postThread, final IAccountRepository repository) {
		return new SetAccountUseCase(executor, postThread, repository);
	}

	@Singleton
	@Provides
	GetCredentialsUseCase providesGetCredentialUseCase(final ThreadExecutor executor, final PostThreadExecutor postThread, final IAccountRepository repository) {
		return new GetCredentialsUseCase(executor, postThread, repository);
	}

	@Singleton
	@Provides
	GetBatchWeightDataUseCase providesGetBatchWeightDataUseCase(final ThreadExecutor executor, final PostThreadExecutor postExecutor, final IWeightRepository repo) {
		return new GetBatchWeightDataUseCase(executor, postExecutor, repo);
	}

}
