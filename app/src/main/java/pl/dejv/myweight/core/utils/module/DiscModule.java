package pl.dejv.myweight.core.utils.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.account.data.AccountDataSourceDisk;
import pl.dejv.myweight.account.data.IAccountDataSource;
import pl.dejv.myweight.core.data.IGoogleSheetClient;

/**
 * Created by dejv on 02/09/2017.
 */
@Module
public class DiscModule {

	@Singleton
	@Provides
	IAccountDataSource providesAccountDataSource(IGoogleSheetClient client) {
		return new AccountDataSourceDisk(client);
	}
}
