package pl.dejv.myweight.core.utils.module;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.account.domain.SetAccountUseCase;
import pl.dejv.myweight.account.presenter.AccountPresenterImpl;
import pl.dejv.myweight.account.presenter.IAccountPresenter;
import pl.dejv.myweight.core.utils.annotation.PerActivity;
import pl.dejv.myweight.createSheet.domain.CreateSheetUseCase;
import pl.dejv.myweight.createSheet.domain.GetCredentialsUseCase;
import pl.dejv.myweight.createSheet.presenter.CreateSheetPresenterImpl;
import pl.dejv.myweight.createSheet.presenter.ICreateSheetPresenter;
import pl.dejv.myweight.weight.domain.GetBatchWeightDataUseCase;
import pl.dejv.myweight.weight.domain.PostWeightUseCase;
import pl.dejv.myweight.weight.presenter.IMyWeightPresenter;
import pl.dejv.myweight.weight.presenter.MyWeightPresenterImpl;

/**
 * Created by Dawid Krężel on 12/02/2017.
 */
@Module
public class PresentationModule {

	@PerActivity
	@Provides
	IMyWeightPresenter providesMyWeightPresenter(final PostWeightUseCase postWeightUseCase, final GetBatchWeightDataUseCase getBatchWeightUseCase) {
		return new MyWeightPresenterImpl(postWeightUseCase, getBatchWeightUseCase);
	}

	@PerActivity
	@Provides
	ICreateSheetPresenter providesCreateSheetPresenter(final CreateSheetUseCase useCase) {
		return new CreateSheetPresenterImpl(useCase);
	}

	@PerActivity
	@Provides
	IAccountPresenter providesAccountPresenter(final SetAccountUseCase accountUseCase, final GetCredentialsUseCase credentialsUseCase) {
		return new AccountPresenterImpl(accountUseCase, credentialsUseCase);
	}
}
