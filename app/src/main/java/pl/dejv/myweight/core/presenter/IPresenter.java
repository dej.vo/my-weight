package pl.dejv.myweight.core.presenter;

/**
 * Created by dejv on 12/02/2017.
 */
public interface IPresenter<T> {

	void bind(T view);

	void unbind();
}
