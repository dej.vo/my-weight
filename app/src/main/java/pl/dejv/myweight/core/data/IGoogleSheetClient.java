package pl.dejv.myweight.core.data;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.sheets.v4.Sheets;

/**
 * Created by dejv on 02/09/2017.
 */

public interface IGoogleSheetClient {

	GoogleAccountCredential setAccountName(final String accountName);

	Sheets createService(final HttpTransport http, final JsonFactory jsonFactory);

	GoogleAccountCredential getCredentials();
}
