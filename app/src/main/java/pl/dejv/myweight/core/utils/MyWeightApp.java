package pl.dejv.myweight.core.utils;

import android.support.multidex.MultiDexApplication;

import pl.dejv.myweight.core.utils.component.ApplicationComponent;
import pl.dejv.myweight.core.utils.component.DaggerApplicationComponent;
import pl.dejv.myweight.core.utils.module.ApplicationModule;

/**
 * Created by dejv on 05/02/2017.
 */

public class MyWeightApp extends MultiDexApplication {

    private static MyWeightApp mInstance;
    private ApplicationComponent mAppComponent;

    public MyWeightApp() {
    }

    public static MyWeightApp getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        setupApplicationComponent();
    }

    private void setupApplicationComponent() {
        mAppComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return mAppComponent;
    }

}
