package pl.dejv.myweight.core.utils;

/**
 * Created by Dawid Krężel on 12/02/2017.
 */
public interface MyWeightCommonInterface {

	int REQUEST_GOOGLE_PLAY_SERVICES = 1000;
	int REQUEST_PERMISSION_GET_ACCOUNT = 1001;
	int REQUEST_GOOGLE_ACCOUNT_PICKER = 1002;
}
