package pl.dejv.myweight.core.data;

import android.content.Context;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import pl.dejv.myweight.core.utils.MyUtils;

/**
 * Created by dejv on 02/09/2017.
 */

public class GoogleSheetClient implements IGoogleSheetClient {

	private static final String[] SCOPES = {SheetsScopes.SPREADSHEETS};
	private GoogleAccountCredential credential;

	public GoogleSheetClient(Context context) {
		setupCredentials(context);
	}

	private void setupCredentials(final Context context) {
		credential = GoogleAccountCredential.usingOAuth2(
				context, Arrays.asList(SCOPES))
				.setBackOff(new ExponentialBackOff());
		credential.setSelectedAccountName(MyUtils.getSelectedAccountName(context));
	}

	@Override
	public GoogleAccountCredential setAccountName(String accountName) {
		return credential.setSelectedAccountName(accountName);
	}

	@Override
	public Sheets createService(HttpTransport http, JsonFactory jsonFactory) {
		Logger.getLogger(HttpTransport.class.getName()).setLevel(Level.ALL);
		return new Sheets.Builder(http, jsonFactory, credential)
				.setApplicationName("MyWeight")
				.build();
	}

	@Override
	public GoogleAccountCredential getCredentials() {
		return credential;
	}
}
