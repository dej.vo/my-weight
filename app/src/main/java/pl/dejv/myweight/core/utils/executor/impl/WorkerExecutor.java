package pl.dejv.myweight.core.utils.executor.impl;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import pl.dejv.myweight.core.utils.executor.ThreadExecutor;

/**
 * Created by dejv on 05/02/2017.
 */
@Singleton
public class WorkerExecutor implements ThreadExecutor {

	private final ThreadPoolExecutor threadPoolExecutor;

	@Inject
	WorkerExecutor() {
		threadPoolExecutor = new ThreadPoolExecutor(3, 5, 10, TimeUnit.SECONDS,
				new LinkedBlockingQueue<Runnable>(), Executors.defaultThreadFactory());
	}

	@Override
	public void execute(Runnable runnable) {
		threadPoolExecutor.execute(runnable);
	}

}
