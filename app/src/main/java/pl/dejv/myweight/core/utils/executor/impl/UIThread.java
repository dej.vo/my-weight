package pl.dejv.myweight.core.utils.executor.impl;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;

/**
 * Created by dejv on 05/02/2017.
 */
@Singleton
public class UIThread implements PostThreadExecutor {

	@Inject
	UIThread() {
	}

	@Override
	public Scheduler getScheduler() {
		return AndroidSchedulers.mainThread();
	}
}
