package pl.dejv.myweight.core.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import pl.dejv.myweight.core.utils.MyWeightApp;
import pl.dejv.myweight.core.utils.component.ActivityComponent;
import pl.dejv.myweight.core.utils.component.ApplicationComponent;
import pl.dejv.myweight.core.utils.component.DaggerActivityComponent;
import pl.dejv.myweight.core.utils.module.ActivityModule;

/**
 * Created by dejv on 02/09/2017.
 */

public abstract class AbstractFragment extends Fragment {
	protected ApplicationComponent mAppComponent;
	protected ActivityComponent mActivityComponent;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		injectComponents();
		injectDependencies();
	}

	private void injectComponents() {
		mAppComponent = MyWeightApp.getInstance().getApplicationComponent();
		mActivityComponent = DaggerActivityComponent.builder()
				.applicationComponent(mAppComponent)
				.activityModule(new ActivityModule(getActivity()))
				.build();
	}

	protected abstract void injectDependencies();

}
