package pl.dejv.myweight.core.utils.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.createSheet.data.SheetResponseMapper;
import pl.dejv.myweight.createSheet.data.SheetResponseMapperImpl;
import pl.dejv.myweight.weight.data.WeightResponseMapper;
import pl.dejv.myweight.weight.data.WeightResponseMapperImpl;

/**
 * Created by Dawid Krężel on 14/02/2017.
 */
@Module
public class ResponseMapperModule {

	@Singleton
	@Provides
	WeightResponseMapper providesWeightResponseMapper() {
		return new WeightResponseMapperImpl();
	}

	@Singleton
	@Provides
	SheetResponseMapper provideSheetResponseMapper() {
		return new SheetResponseMapperImpl();
	}
}
