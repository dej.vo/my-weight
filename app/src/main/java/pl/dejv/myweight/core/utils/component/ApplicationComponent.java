package pl.dejv.myweight.core.utils.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import pl.dejv.myweight.account.data.IAccountRepository;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;
import pl.dejv.myweight.core.utils.module.ApplicationModule;
import pl.dejv.myweight.core.utils.module.DiscModule;
import pl.dejv.myweight.core.utils.module.DomainModule;
import pl.dejv.myweight.core.utils.module.NetworkModule;
import pl.dejv.myweight.core.utils.module.RepositoryModule;
import pl.dejv.myweight.core.utils.module.ResponseMapperModule;
import pl.dejv.myweight.core.utils.module.APIModule;
import pl.dejv.myweight.createSheet.data.ISheetRepository;
import pl.dejv.myweight.weight.data.IWeightRepository;

/**
 * Created by dejv on 05/02/2017.
 */
@Singleton
@Component(modules = {
		ApplicationModule.class,
		RepositoryModule.class,
		DiscModule.class,
		DomainModule.class,
		APIModule.class,
		ResponseMapperModule.class,
		NetworkModule.class})
public interface ApplicationComponent {

	Context getContext();

	ThreadExecutor getThreadExecutor();

	PostThreadExecutor getPostThreadExecutor();

	IWeightRepository provideWeighRepository();

	ISheetRepository provideSheetRepository();

	IAccountRepository provideAccountRepository();
}
