package pl.dejv.myweight.core.utils.module;

import com.google.api.services.sheets.v4.Sheets;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.dejv.myweight.createSheet.data.SheetDataSource;
import pl.dejv.myweight.createSheet.data.SheetDataSourceNetwork;
import pl.dejv.myweight.createSheet.data.SheetResponseMapper;
import pl.dejv.myweight.weight.data.WeightDataSource;
import pl.dejv.myweight.weight.data.WeightDataSourceNetwork;
import pl.dejv.myweight.weight.data.WeightResponseMapper;

/**
 * Created by dejv on 14/02/2017.
 */
@Module
public class NetworkModule {

	@Singleton
	@Provides
	WeightDataSource providesWeightDataSource(final Sheets api, WeightResponseMapper mapper) {
		return new WeightDataSourceNetwork(api, mapper);
	}

	@Singleton
	@Provides
	SheetDataSource providesSheetDataSource(final Sheets api, SheetResponseMapper mapper) {
		return new SheetDataSourceNetwork(api, mapper);
	}
}
