package pl.dejv.myweight;

/**
 * Created by dejv on 12/02/2017.
 */

public interface MyWeightView {
	void onComplete();

	void onError(String message);
}
