package pl.dejv.myweight.account.presenter;

import java.io.IOException;

import javax.inject.Inject;

import pl.dejv.myweight.account.domain.SetAccountUseCase;
import pl.dejv.myweight.account.view.GetCredentialsSubscriber;
import pl.dejv.myweight.account.view.ISetAccountView;
import pl.dejv.myweight.account.view.SetAccountNameSubscriber;
import pl.dejv.myweight.core.presenter.AbstractPresenter;
import pl.dejv.myweight.createSheet.domain.GetCredentialsUseCase;

/**
 * Created by dejv on 02/09/2017.
 */

public class AccountPresenterImpl extends AbstractPresenter<ISetAccountView> implements IAccountPresenter {

	private final SetAccountUseCase setAccountUseCase;
	private final GetCredentialsUseCase credentialsUseCase;

	@Inject
	public AccountPresenterImpl(SetAccountUseCase useCase, GetCredentialsUseCase credentialsUseCase) {
		this.setAccountUseCase = useCase;
		this.credentialsUseCase = credentialsUseCase;
	}

	@Override
	protected void onBind() {

	}

	@Override
	protected void onUnbind() {

	}

	@Override
	public void setAccountName(String accountName) throws IOException {
		setAccountUseCase.execute(new SetAccountNameSubscriber(view), accountName);
	}

	@Override
	public void getCredentials() throws IOException {
		credentialsUseCase.execute(new GetCredentialsSubscriber(view), null);
	}

}
