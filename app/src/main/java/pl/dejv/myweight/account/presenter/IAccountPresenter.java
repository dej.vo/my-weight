package pl.dejv.myweight.account.presenter;

import java.io.IOException;

import pl.dejv.myweight.account.view.ISetAccountView;
import pl.dejv.myweight.core.presenter.IPresenter;

/**
 * Created by dejv on 02/09/2017.
 */

public interface IAccountPresenter extends IPresenter<ISetAccountView> {

	void setAccountName(final String accountName) throws IOException;

	void getCredentials() throws IOException;
}
