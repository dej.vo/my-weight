package pl.dejv.myweight.account.view;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by dejv on 28/09/2017.
 */

public class RedirectSettingsActivity extends Activity {

    public static final String PATH_SETTINGS = "settings";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent iRedirect = new Intent(Intent.ACTION_EDIT, createUriBuilder(PATH_SETTINGS).build());
        iRedirect.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        startActivity(iRedirect);
        finish();
    }

    private Uri.Builder createUriBuilder(String path) {
        final Uri.Builder builder = new Uri.Builder();
        return builder
                .scheme("content")
                .authority("myweight.dejv.pl")
                .path(path);
    }

}
