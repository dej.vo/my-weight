package pl.dejv.myweight.account.domain;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import pl.dejv.myweight.account.data.IAccountRepository;
import pl.dejv.myweight.core.domain.UseCase;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;

/**
 * Created by dejv on 02/09/2017.
 */

public class SetAccountUseCase extends UseCase<GoogleAccountCredential, String> {

	private final IAccountRepository accountRepository;

	@Inject
	public SetAccountUseCase(ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, IAccountRepository repository) {
		super(threadExecutor, postThreadExecutor);
		accountRepository = repository;
	}

	@Override
	public Observable<GoogleAccountCredential> createObservable(String accountName) throws IOException {
		return accountRepository.saveAccountName(accountName);
	}

}
