package pl.dejv.myweight.account.view;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by dejv on 02/09/2017.
 */

public class SetAccountNameSubscriber extends DisposableObserver<GoogleAccountCredential> {

	private final ISetAccountView view;

	public SetAccountNameSubscriber(ISetAccountView view) {
		this.view = view;
	}

	@Override
	public void onNext(GoogleAccountCredential value) {
		view.onComplete(value);
	}

	@Override
	public void onError(Throwable e) {
		view.onError(e);
	}

	@Override
	public void onComplete() {

	}

}
