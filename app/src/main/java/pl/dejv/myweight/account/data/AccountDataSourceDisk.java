package pl.dejv.myweight.account.data;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import javax.inject.Inject;

import io.reactivex.Observable;
import pl.dejv.myweight.core.data.IGoogleSheetClient;

/**
 * Created by dejv on 02/09/2017.
 */

public class AccountDataSourceDisk implements IAccountDataSource {

	private final IGoogleSheetClient client;

	@Inject
	public AccountDataSourceDisk(IGoogleSheetClient client) {
		this.client = client;
	}

	@Override
	public Observable<GoogleAccountCredential> saveAccountName(String name) {
		return Observable.just(client.setAccountName(name));
	}

	@Override
	public Observable<GoogleAccountCredential> getCredentials() {
		return Observable.just(client.getCredentials());
	}

}
