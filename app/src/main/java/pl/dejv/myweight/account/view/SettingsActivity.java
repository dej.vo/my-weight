package pl.dejv.myweight.account.view;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import java.io.IOException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.dejv.myweight.R;
import pl.dejv.myweight.account.presenter.IAccountPresenter;
import pl.dejv.myweight.core.utils.MyWeightApp;
import pl.dejv.myweight.core.utils.MyWeightCommonInterface;
import pl.dejv.myweight.core.utils.component.ActivityComponent;
import pl.dejv.myweight.core.utils.component.ApplicationComponent;
import pl.dejv.myweight.core.utils.component.DaggerActivityComponent;
import pl.dejv.myweight.core.utils.module.ActivityModule;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by dejv on 13/09/2017.
 */

public class SettingsActivity extends AppCompatActivity implements ISetAccountView {

	private ApplicationComponent mAppComponent;
	private ActivityComponent mActivityComponent;
	private GoogleAccountCredential mGoogleAccountCredentials;

	@Inject
	IAccountPresenter mAccountPresenter;

	@BindView(R.id.choose_account)
	Button mChooseAccountBtn;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_layout);

		mAppComponent = MyWeightApp.getInstance().getApplicationComponent();
		mActivityComponent = DaggerActivityComponent.builder()
				.applicationComponent(mAppComponent)
				.activityModule(new ActivityModule(this))
				.build();
		mActivityComponent.inject(this);
		ButterKnife.bind(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mAccountPresenter.bind(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mAccountPresenter.unbind();
	}

	@OnClick(R.id.choose_account)
	public void onChooseAccountClick() {
		try {
			mAccountPresenter.getCredentials();
		} catch (IOException e) {
			onError(e);
		}
	}

	@Override
	public void onError(Throwable error) {

	}

	@Override
	public void onComplete(GoogleAccountCredential credential) {
		mGoogleAccountCredentials = credential;
		chooseGoogleAccount();
	}

	private void chooseGoogleAccount() {
		if (EasyPermissions.hasPermissions(this, Manifest.permission.GET_ACCOUNTS)) {
			startActivityForResult(
					mGoogleAccountCredentials.newChooseAccountIntent(),
					MyWeightCommonInterface.REQUEST_GOOGLE_ACCOUNT_PICKER);
		} else {
			EasyPermissions.requestPermissions(this,
					"This app needs to access your Google account",
					MyWeightCommonInterface.REQUEST_PERMISSION_GET_ACCOUNT,
					Manifest.permission.GET_ACCOUNTS);
		}
	}

}
