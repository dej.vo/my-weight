package pl.dejv.myweight.account.data;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by dejv on 02/09/2017.
 */
@Singleton
public class AccountRepositoryImpl implements IAccountRepository {

	private final IAccountDataSource source;

	@Inject
	public AccountRepositoryImpl(IAccountDataSource source) {
		this.source = source;
	}

	@Override
	public Observable<GoogleAccountCredential> saveAccountName(String name) {
		return source.saveAccountName(name);
	}

	@Override
	public Observable<GoogleAccountCredential> getCredentials() {
		return source.getCredentials();
	}
}
