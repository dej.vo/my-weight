package pl.dejv.myweight.account.view;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import pl.dejv.myweight.core.view.IMyWeightView;

/**
 * Created by dejv on 02/09/2017.
 */

public interface ISetAccountView extends IMyWeightView {
	void onComplete(final GoogleAccountCredential credential);
}
