package pl.dejv.myweight.account.data;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by dejv on 02/09/2017.
 */
@Singleton
public interface IAccountRepository {

	Observable<GoogleAccountCredential> saveAccountName(final String name);

	Observable<GoogleAccountCredential> getCredentials();
}
