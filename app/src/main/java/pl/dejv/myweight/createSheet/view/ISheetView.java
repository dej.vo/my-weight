package pl.dejv.myweight.createSheet.view;

import com.google.api.services.sheets.v4.model.Spreadsheet;

import pl.dejv.myweight.core.view.IMyWeightView;

/**
 * Created by dejv on 30/08/2017.
 */

public interface ISheetView extends IMyWeightView {
	void onComplete(Spreadsheet sheet);
}
