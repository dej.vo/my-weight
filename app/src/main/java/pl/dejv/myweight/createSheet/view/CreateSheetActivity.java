package pl.dejv.myweight.createSheet.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.ButterKnife;
import pl.dejv.myweight.R;
import pl.dejv.myweight.account.view.SettingsActivity;
import pl.dejv.myweight.core.view.AbstractActivity;

/**
 * Created by Dawid Krężel on 12/02/2017.
 */

public class CreateSheetActivity extends AbstractActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		ButterKnife.bind(this);
	}

	@Override
	protected void injectDependencies() {
		mActivityComponent.inject(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.settings_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final int itemId = item.getItemId();
		if (R.id.settings == itemId) {
			final Intent i = new Intent(this, SettingsActivity.class);
			startActivity(i);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
