package pl.dejv.myweight.createSheet.view;

import com.google.api.services.sheets.v4.model.Spreadsheet;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by dejv on 30/08/2017.
 */

public class CreateSheetSubscriber extends DisposableObserver<Spreadsheet> {

	private final ISheetView view;

	public CreateSheetSubscriber(ISheetView view) {
		this.view = view;
	}

	@Override
	public void onNext(Spreadsheet value) {
		view.onComplete(value);
	}

	@Override
	public void onError(Throwable e) {
		view.onError(e);
	}

	@Override
	public void onComplete() {
	}

}
