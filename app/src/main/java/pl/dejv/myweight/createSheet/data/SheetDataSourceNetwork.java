package pl.dejv.myweight.createSheet.data;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Spreadsheet;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by dejv on 30/08/2017.
 */

public class SheetDataSourceNetwork implements SheetDataSource {

	private final Sheets service;
	private final SheetResponseMapper mapper;

	@Inject
	public SheetDataSourceNetwork(Sheets service, SheetResponseMapper mapper) {
		this.service = service;
		this.mapper = mapper;
	}

	@Override
	public Observable<Sheets.Spreadsheets.Create> createSheet(Spreadsheet spreadsheet) throws IOException {
		return Observable.just(service.spreadsheets().create(spreadsheet));
	}

}
