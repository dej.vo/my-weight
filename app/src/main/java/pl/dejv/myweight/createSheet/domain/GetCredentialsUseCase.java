package pl.dejv.myweight.createSheet.domain;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import pl.dejv.myweight.account.data.IAccountRepository;
import pl.dejv.myweight.core.domain.UseCase;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;

/**
 * Created by dejv on 02/09/2017.
 */

public class GetCredentialsUseCase extends UseCase<GoogleAccountCredential, Void> {

	private final IAccountRepository repository;

	@Inject
	public GetCredentialsUseCase(ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, IAccountRepository repository) {
		super(threadExecutor, postThreadExecutor);
		this.repository = repository;
	}

	@Override
	public Observable<GoogleAccountCredential> createObservable(Void aVoid) throws IOException {
		return repository.getCredentials();
	}

}
