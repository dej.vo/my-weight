package pl.dejv.myweight.createSheet.view;

import android.Manifest;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.services.sheets.v4.model.Spreadsheet;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.dejv.myweight.R;
import pl.dejv.myweight.account.presenter.IAccountPresenter;
import pl.dejv.myweight.account.view.ISetAccountView;
import pl.dejv.myweight.core.utils.HandleError;
import pl.dejv.myweight.core.utils.MyUtils;
import pl.dejv.myweight.core.utils.MyWeightCommonInterface;
import pl.dejv.myweight.core.view.AbstractFragment;
import pl.dejv.myweight.createSheet.presenter.ICreateSheetPresenter;
import pl.dejv.myweight.weight.presenter.IMyWeightPresenter;
import pl.dejv.myweight.weight.view.IWeightDataView;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;

/**
 * Created by dejv on 31/08/2017.
 */

public class CreateSheetFragment extends AbstractFragment implements ISheetView, ISetAccountView, IWeightDataView, EasyPermissions.PermissionCallbacks {

	public static CreateSheetFragment instance() {
		return new CreateSheetFragment();
	}

	@BindView(R.id.weight_input)
	EditText mWeightInput;

	@BindView(R.id.choose_account)
	Button mChooseAccountBtn;

	@BindView(R.id.create_sheet)
	Button mCreateSheetBtn;

	@BindView(R.id.save_weight)
	Button mSaveWeightBtn;

	@BindView(R.id.choose_sheet)
	Button mChooseSheetBtn;

	@BindView(R.id.sheet_navigate_container)
	LinearLayout mSheetNavigatorContainer;

	@BindView(R.id.content_data)
	TextView mContentData;

	@BindView(R.id.weight_data_list)
	ListView mWeightDataList;

	@Inject
	ICreateSheetPresenter mCreateSheetPresenter;

	@Inject
	IAccountPresenter mAccountPresenter;

	@Inject
	IMyWeightPresenter mWeightPresenter;

	private GoogleAccountCredential mAccountCredential;
	private static final int REQUEST_AUTHORIZATION = 1;
	private ArrayAdapter<Object> mAdapter;

	private Unbinder unbinder;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCreateSheetPresenter.bind(this);
		mAccountPresenter.bind(this);
		mWeightPresenter.bind(this);

		mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);
	}

	@Override
	protected void injectDependencies() {
		mActivityComponent.inject(this);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.create_sheet, container, false);
		unbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mWeightDataList.setAdapter(mAdapter);
		try {
			mAccountPresenter.getCredentials();
		} catch (IOException e) {
			onError(e);
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (unbinder != null) {
			unbinder.unbind();
		}
		mCreateSheetPresenter.unbind();
		mAccountPresenter.unbind();
		mWeightPresenter.unbind();
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case MyWeightCommonInterface.REQUEST_GOOGLE_ACCOUNT_PICKER:
				if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
					final String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
					if (accountName != null) {
						try {
							mChooseAccountBtn.setVisibility(View.GONE);
							mSheetNavigatorContainer.setVisibility(View.VISIBLE);
							mAccountPresenter.setAccountName(accountName);
						} catch (IOException e) {
							onError(e);
						}
					}
				} else {
					mWeightInput.setText("This app needs to choose Google Account.");
					mChooseAccountBtn.setEnabled(true);
				}
				break;
			case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
				checkNeededServices();
				break;
		}
	}

	@Override
	public void onPermissionsGranted(int requestCode, List<String> perms) {
		switch (requestCode) {
			case MyWeightCommonInterface.REQUEST_PERMISSION_GET_ACCOUNT:
				checkNeededServices();
				break;
			case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
				checkNeededServices();
				break;
		}
	}

	@Override
	public void onPermissionsDenied(int requestCode, List<String> perms) {
		switch (requestCode) {
			case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
				mWeightInput.setText("This app will not work properly unless you grant account permission.");
				break;
			case MyWeightCommonInterface.REQUEST_PERMISSION_GET_ACCOUNT:
				mWeightInput.setText("This app needs account permission to work properly.");
				break;
		}
		if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
			new AppSettingsDialog.Builder(this,
					"Your checked `Never ask again` for permission that is important for this app to work properly.")
					.setNegativeButton("Cancel",
							(dialog, which) ->
									mWeightInput.setText("This app will not work properly unless you grant account permission."))
					.build()
					.show();
		}
	}

	@OnClick(R.id.choose_account)
	public void onChooseAccountClick() {
		chooseGoogleAccount();
	}

	@OnClick(R.id.create_sheet)
	public void onCreateSheetClick() {
		if (isAccountSetup()) {
			try {
				mCreateSheetPresenter.createSheet("My Spreadsheet:" + new Date().toString());
			} catch (IOException e) {
				onError(e);
			}
		}
	}

	@OnClick(R.id.choose_sheet)
	public void onChooseSheetClick() {
		if (hasNeededServices()) {
			try {
				mCreateSheetPresenter.chooseSheet();
			} catch (IOException e) {
				onError(e);
			}
		}
	}

	@OnClick(R.id.save_weight)
	public void onSaveWeightClick() {
		try {
			mWeightPresenter.postWeight(mWeightInput.getText().toString());
		} catch (IOException e) {
			onError(e);
		}
	}

	@Override
	public void onComplete(GoogleAccountCredential credential) {
		MyUtils.saveAccountName(getActivity(), credential.getSelectedAccountName());    // FIXME : should be done on worker thread
		mAccountCredential = credential;
		mAccountCredential.setSelectedAccountName(credential.getSelectedAccountName());
		if (checkNeededServices()) {
			try {
				mWeightPresenter.getBatchWeightData();
			} catch (IOException e) {
				onError(e);
			}
		}
	}

	@Override
	public void onComplete(Spreadsheet sheet) {
		MyUtils.saveSpreadsheetId(getActivity(), sheet);
		showWeightInputBtn();
		mWeightInput.setText("");
	}

	@Override
	public void onError(Throwable error) {
		if (error == null) {
			MyUtils.notImplemented(this.getView());
		} else if (error instanceof UserRecoverableAuthIOException) {
			Intent intent = ((UserRecoverableAuthIOException) error).getIntent();
			startActivityForResult(intent, REQUEST_AUTHORIZATION);
		} else {
			HandleError.showMsg(error, this.getView());
		}
	}

	private boolean checkNeededServices() {
		boolean hasNeededServices = false;
		if (!MyUtils.isNetworkConnection(getActivity())) {
			// FIXME : handle no internet connection in a subtle way, i.e.: show Snackbar or Dialog
			mWeightInput.setText("No internet connection!");
		} else if (!MyUtils.isGooglePlayServicesAvailable(getActivity())) {
			MyUtils.acquireGooglePlayServices((AppCompatActivity) getActivity());
		} else if (mAccountCredential.getSelectedAccountName() == null) {
			chooseGoogleAccount();
		} else if (MyUtils.getSpreadsheetId(getActivity()) == null) {
			mChooseAccountBtn.setVisibility(View.GONE);
			mSheetNavigatorContainer.setVisibility(View.VISIBLE);
			mWeightInput.setText("Create Your first spreadsheet.");
		} else {
			showWeightInputBtn();
			hasNeededServices = true;
		}
		return hasNeededServices;
	}

	private boolean isSpreadsheetIdCreated() {
		final String spreadsheetId = MyUtils.getSpreadsheetId(getActivity());
		if (spreadsheetId == null) {
			return false;
		}
		return true;
	}

	private boolean isNetworkAvailable() {
		final boolean result = MyUtils.isNetworkConnection(getActivity());
		if (!result) {
			// FIXME : handle no internet connection in a subtle way, i.e.: show Snackbar or Dialog
			mWeightInput.setText("No internet connection!");
		}
		return result;
	}

	private boolean isGooglePlayServicesAvailable() {
		final boolean result = MyUtils.isGooglePlayServicesAvailable(getActivity());
		if (!result) {
			MyUtils.acquireGooglePlayServices((AppCompatActivity) getActivity());
		}
		return result;
	}

	private boolean isAccountSetup() {
		if (!hasNeededServices()) {
			return false;
		} else if (mAccountCredential.getSelectedAccountName() == null) {
			chooseGoogleAccount();
			return false;
		} else {
			return true;
		}
	}

	private boolean hasNeededServices() {
		return isNetworkAvailable() && isGooglePlayServicesAvailable();
	}

	private void chooseGoogleAccount() {
		if (EasyPermissions.hasPermissions(getActivity(), Manifest.permission.GET_ACCOUNTS)) {
			if (mAccountCredential.getSelectedAccountName() == null) {
				startActivityForResult(
						mAccountCredential.newChooseAccountIntent(),
						MyWeightCommonInterface.REQUEST_GOOGLE_ACCOUNT_PICKER);
			} else {
				checkNeededServices();
			}
		} else {
			EasyPermissions.requestPermissions(this,
					"This app needs to access your Google account",
					MyWeightCommonInterface.REQUEST_PERMISSION_GET_ACCOUNT,
					Manifest.permission.GET_ACCOUNTS);
		}
	}

	private void showWeightInputBtn() {
		mChooseAccountBtn.setVisibility(View.GONE);
		mSheetNavigatorContainer.setVisibility(View.GONE);
		mSaveWeightBtn.setVisibility(View.VISIBLE);
		mWeightInput.setClickable(true);
		mWeightInput.setEnabled(true);
	}

	@Override
	public void onComplete(String data) {
		mAdapter.add(data);
		mAdapter.notifyDataSetChanged();
		mWeightInput.setText("");
	}

	@Override
	public void onComplete(Collection<Object> weightList) {
		mAdapter.clear();
		mAdapter.addAll(weightList);
		mAdapter.notifyDataSetChanged();
	}
}
