package pl.dejv.myweight.createSheet.data;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Spreadsheet;

import java.io.IOException;

import javax.inject.Singleton;

import io.reactivex.Observable;
import okhttp3.Response;

/**
 * Created by dejv on 30/08/2017.
 */

@Singleton
public interface SheetDataSource {

	Observable<Sheets.Spreadsheets.Create> createSheet(final Spreadsheet spreadsheet) throws IOException;
}
