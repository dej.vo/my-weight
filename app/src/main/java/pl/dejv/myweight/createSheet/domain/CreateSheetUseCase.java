package pl.dejv.myweight.createSheet.domain;

import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.services.sheets.v4.model.Spreadsheet;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import pl.dejv.myweight.core.domain.UseCase;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;
import pl.dejv.myweight.createSheet.data.ISheetRepository;

/**
 * Created by dejv on 30/08/2017.
 */

public class CreateSheetUseCase extends UseCase<Spreadsheet, Spreadsheet> {

	private final ISheetRepository sheetRepository;

	@Inject
	public CreateSheetUseCase(ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, ISheetRepository sheetRepository) {
		super(threadExecutor, postThreadExecutor);
		this.sheetRepository = sheetRepository;
	}

	@Override
	public Observable<Spreadsheet> createObservable(Spreadsheet spreadsheet) throws IOException {
		return sheetRepository.createSheet(spreadsheet)
				.map(AbstractGoogleClientRequest::execute);
	}

}
