package pl.dejv.myweight.createSheet.presenter;

import java.io.IOException;

import pl.dejv.myweight.core.presenter.IPresenter;
import pl.dejv.myweight.createSheet.view.ISheetView;

/**
 * Created by dejv on 30/08/2017.
 */

public interface ICreateSheetPresenter extends IPresenter<ISheetView> {

	void createSheet(final String name) throws IOException;

	void chooseSheet() throws IOException;
}
