package pl.dejv.myweight.createSheet.data;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.Spreadsheet;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by dejv on 30/08/2017.
 */

public class SheetRepositoryImpl implements ISheetRepository {

	private final SheetDataSource networkDataSource;

	@Inject
	public SheetRepositoryImpl(SheetDataSource networkDataSource) {
		this.networkDataSource = networkDataSource;
	}

	@Override
	public Observable<Sheets.Spreadsheets.Create> createSheet(Spreadsheet spreadsheet) throws IOException {
		return networkDataSource.createSheet(spreadsheet);
	}

}
