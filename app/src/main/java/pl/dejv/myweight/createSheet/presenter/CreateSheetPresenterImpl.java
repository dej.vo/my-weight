package pl.dejv.myweight.createSheet.presenter;

import com.google.api.services.sheets.v4.model.Spreadsheet;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;

import java.io.IOException;

import javax.inject.Inject;

import pl.dejv.myweight.core.presenter.AbstractPresenter;
import pl.dejv.myweight.createSheet.domain.CreateSheetUseCase;
import pl.dejv.myweight.createSheet.view.CreateSheetSubscriber;
import pl.dejv.myweight.createSheet.view.ISheetView;

/**
 * Created by dejv on 30/08/2017.
 */

public class CreateSheetPresenterImpl extends AbstractPresenter<ISheetView> implements ICreateSheetPresenter {

	private final CreateSheetUseCase useCase;

	@Inject
	public CreateSheetPresenterImpl(CreateSheetUseCase useCase) {
		this.useCase = useCase;
	}

	@Override
	public void createSheet(String name) throws IOException {
		final Spreadsheet spreadsheet = new Spreadsheet();
		final SpreadsheetProperties spreadsheetProp = new SpreadsheetProperties().setTitle(name);
		spreadsheet.setProperties(spreadsheetProp);

		useCase.execute(new CreateSheetSubscriber(view), spreadsheet);
	}

	@Override
	public void chooseSheet() throws IOException {
		view.onError(null);
	}

	@Override
	protected void onBind() {

	}

	@Override
	protected void onUnbind() {

	}

}
