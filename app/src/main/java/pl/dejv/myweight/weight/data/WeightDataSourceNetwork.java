package pl.dejv.myweight.weight.data;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Dawid Krężel on 07/02/2017.
 */
public class WeightDataSourceNetwork implements WeightDataSource {

	private final Sheets service;
	private final WeightResponseMapper weightResponseMapper;

	@Inject
	public WeightDataSourceNetwork(Sheets sheets, WeightResponseMapper weightResponseMapper) {
		this.service = sheets;
		this.weightResponseMapper = weightResponseMapper;
	}

	@Override
	public Observable<Sheets.Spreadsheets.Values.Append> postNewWeight(String spreadSheetId, String range, ValueRange values) throws IOException {
		String valueInputOption = "RAW";
		return Observable.just(service
				.spreadsheets()
				.values()
				.append(spreadSheetId, range, values)
				.setIncludeValuesInResponse(true)
				.setValueInputOption(valueInputOption));
	}

	@Override
	public Observable<Sheets.Spreadsheets.Values.BatchGet> getBatchWeight(String spreadSheetId, List<String> ranges) throws IOException {

		return Observable.just(service
				.spreadsheets()
				.values()
				.batchGet(spreadSheetId)
				.setRanges(ranges));
	}
}
