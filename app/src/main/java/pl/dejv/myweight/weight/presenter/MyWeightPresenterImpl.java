package pl.dejv.myweight.weight.presenter;

import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pl.dejv.myweight.core.presenter.AbstractPresenter;
import pl.dejv.myweight.core.utils.MyUtils;
import pl.dejv.myweight.weight.domain.GetBatchWeightDataMapper;
import pl.dejv.myweight.weight.domain.GetBatchWeightDataUseCase;
import pl.dejv.myweight.weight.domain.InputWeightDataMapper;
import pl.dejv.myweight.weight.domain.PostWeightUseCase;
import pl.dejv.myweight.weight.view.GetBatchWeightDataSubscriber;
import pl.dejv.myweight.weight.view.IWeightDataView;
import pl.dejv.myweight.weight.view.PostWeightSubscriber;

/**
 * Created by dejv on 12/02/2017.
 */
public class MyWeightPresenterImpl extends AbstractPresenter<IWeightDataView> implements IMyWeightPresenter {

	private final PostWeightUseCase postWeightUseCase;
	private final GetBatchWeightDataUseCase getBatchWeightUseCase;

	@Inject
	public MyWeightPresenterImpl(PostWeightUseCase postWeightUseCase, GetBatchWeightDataUseCase getBatchWeightUseCase) {
		this.postWeightUseCase = postWeightUseCase;
		this.getBatchWeightUseCase = getBatchWeightUseCase;
	}

	@Override
	protected void onBind() {

	}

	@Override
	protected void onUnbind() {
		postWeightUseCase.dispose();
	}

	@Override
	public void postWeight(String weight) throws IOException {
		final InputWeightDataMapper mapper = new InputWeightDataMapper();
		mapper.setSpreadSheetId(MyUtils.getSpreadsheetId(context));
		mapper.setRange("A1");

		ValueRange values = new ValueRange();

		List<List<Object>> outerArray = new ArrayList<>();
		List<Object> innerArray = new ArrayList<>();

		innerArray.add(weight);
		outerArray.add(innerArray);
		values.setValues(outerArray);

		mapper.setValues(values);

		postWeightUseCase.execute(new PostWeightSubscriber(view), mapper);
	}

	@Override
	public void getBatchWeightData() throws IOException {
		final GetBatchWeightDataMapper mapper = new GetBatchWeightDataMapper();
		final List<String> ranges = new ArrayList<String>() {{
			add("A2:A");    // get all rows from column A except A1 cell.
		}};
		mapper.setSpreadSheetId(MyUtils.getSpreadsheetId(context));
		mapper.setRanges(ranges);

		getBatchWeightUseCase.execute(new GetBatchWeightDataSubscriber(view), mapper);
	}
}
