package pl.dejv.myweight.weight.view;

/**
 * Created by dejv on 29/09/2017.
 */

public class EmptyInputException extends RuntimeException {
    public EmptyInputException(String message) {
        super(message);
    }
}
