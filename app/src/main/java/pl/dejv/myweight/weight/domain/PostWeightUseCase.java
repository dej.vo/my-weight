package pl.dejv.myweight.weight.domain;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.AppendValuesResponse;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import pl.dejv.myweight.core.domain.UseCase;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;
import pl.dejv.myweight.weight.data.IWeightRepository;

/**
 * Created by Dawid Krężel on 07/02/2017.
 */

public class PostWeightUseCase extends UseCase<AppendValuesResponse, InputWeightDataMapper> {

	private final IWeightRepository weightRepository;

	@Inject
	public PostWeightUseCase(ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, IWeightRepository repository) {
		super(threadExecutor, postThreadExecutor);
		weightRepository = repository;
	}

	@Override
	public Observable<AppendValuesResponse> createObservable(InputWeightDataMapper mapper) throws IOException {
		return weightRepository.postNewWeight(mapper.getSpreadSheetId(), mapper.getRange(), mapper.getValues())
				.map(Sheets.Spreadsheets.Values.Append::execute);
	}
}
