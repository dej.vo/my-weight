package pl.dejv.myweight.weight.presenter;

import java.io.IOException;

import pl.dejv.myweight.core.presenter.IPresenter;
import pl.dejv.myweight.weight.view.IWeightDataView;

/**
 * Created by dejv on 12/02/2017.
 */

public interface IMyWeightPresenter extends IPresenter<IWeightDataView> {

	void postWeight(final String weight) throws IOException;

	void getBatchWeightData() throws IOException;
}
