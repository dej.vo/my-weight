package pl.dejv.myweight.weight.view;

import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by dejv on 04/09/2017.
 */

public class GetBatchWeightDataSubscriber extends DisposableObserver<BatchGetValuesResponse> {

	private final IWeightDataView view;

	public GetBatchWeightDataSubscriber(IWeightDataView view) {
		this.view = view;
	}

	@Override
	public void onNext(BatchGetValuesResponse batchData) {
		List<List<Object>> values = batchData.getValueRanges().get(0).getValues();
		if (values != null && !values.isEmpty()) {
			final Iterator<List<Object>> it = values.iterator();
			final List<Object> list = new ArrayList<>();
			while (it.hasNext()) {
				list.add(it.next().get(0));
			}
			view.onComplete(list);
		}
	}

	@Override
	public void onError(Throwable e) {
		view.onError(e);
	}

	@Override
	public void onComplete() {

	}

}
