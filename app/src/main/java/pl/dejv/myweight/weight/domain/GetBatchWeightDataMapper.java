package pl.dejv.myweight.weight.domain;

import java.util.List;

/**
 * Created by dejv on 04/09/2017.
 */

public class GetBatchWeightDataMapper {

	public GetBatchWeightDataMapper() {
	}

	private String spreadSheetId;
	private List<String> ranges;

	public String getSpreadSheetId() {
		return spreadSheetId;
	}

	public void setSpreadSheetId(String spreadSheetId) {
		this.spreadSheetId = spreadSheetId;
	}

	public List<String> getRanges() {
		return ranges;
	}

	public void setRanges(List<String> ranges) {
		this.ranges = ranges;
	}

}
