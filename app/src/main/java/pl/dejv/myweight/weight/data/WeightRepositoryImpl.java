package pl.dejv.myweight.weight.data;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by Dawid Krężel on 07/02/2017.
 */
public class WeightRepositoryImpl implements IWeightRepository {

	private final WeightDataSource networkDataSource;

	@Inject
	public WeightRepositoryImpl(WeightDataSource networkDataSource) {
		this.networkDataSource = networkDataSource;
	}

	@Override
	public Observable<Sheets.Spreadsheets.Values.Append> postNewWeight(String spreadSheetId, String range, ValueRange values) throws IOException {
		return networkDataSource.postNewWeight(spreadSheetId, range, values);
	}

	@Override
	public Observable<Sheets.Spreadsheets.Values.BatchGet> getBatchWeighData(String spreadSheetId, List<String> range) throws IOException {
		return networkDataSource.getBatchWeight(spreadSheetId, range);
	}

}
