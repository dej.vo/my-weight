package pl.dejv.myweight.weight.domain;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.BatchGetValuesResponse;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import pl.dejv.myweight.core.domain.UseCase;
import pl.dejv.myweight.core.utils.executor.PostThreadExecutor;
import pl.dejv.myweight.core.utils.executor.ThreadExecutor;
import pl.dejv.myweight.weight.data.IWeightRepository;

/**
 * Created by dejv on 04/09/2017.
 */

public class GetBatchWeightDataUseCase extends UseCase<BatchGetValuesResponse, GetBatchWeightDataMapper> {

	private final IWeightRepository weightRepository;

	@Inject
	public GetBatchWeightDataUseCase(ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, IWeightRepository repository) {
		super(threadExecutor, postThreadExecutor);
		weightRepository = repository;
	}

	@Override
	public Observable<BatchGetValuesResponse> createObservable(GetBatchWeightDataMapper mapper) throws IOException {
		return weightRepository.getBatchWeighData(mapper.getSpreadSheetId(), mapper.getRanges())
				.map(Sheets.Spreadsheets.Values.BatchGet::execute);
	}

}
