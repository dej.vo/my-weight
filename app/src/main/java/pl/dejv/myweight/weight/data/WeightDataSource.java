package pl.dejv.myweight.weight.data;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.List;

import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by Dawid Krężel on 07/02/2017.
 */
@Singleton
public interface WeightDataSource {

	Observable<Sheets.Spreadsheets.Values.Append> postNewWeight(String spreadSheetId, String range, ValueRange values) throws IOException;

	Observable<Sheets.Spreadsheets.Values.BatchGet> getBatchWeight(final String spreadSheetId, List<String> range) throws IOException;
}
