package pl.dejv.myweight.weight.data;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

import java.io.IOException;
import java.util.List;

import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by Dawid Krężel on 07/02/2017.
 */
@Singleton
public interface IWeightRepository {

	Observable<Sheets.Spreadsheets.Values.Append> postNewWeight(final String spreadSheetId, final String range, final ValueRange values) throws IOException;

	Observable<Sheets.Spreadsheets.Values.BatchGet> getBatchWeighData(final String spreadSheetId, final List<String> range) throws IOException;
}
