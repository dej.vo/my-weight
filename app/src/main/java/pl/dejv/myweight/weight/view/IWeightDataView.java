package pl.dejv.myweight.weight.view;

import java.util.Collection;

import pl.dejv.myweight.core.view.IMyWeightView;

/**
 * Created by dejv on 03/09/2017.
 */

public interface IWeightDataView extends IMyWeightView {

	void onComplete(final String data);

	void onComplete(final Collection<Object> weightList);
}
