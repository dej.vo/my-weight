package pl.dejv.myweight.weight.view;

import com.google.api.services.sheets.v4.model.AppendValuesResponse;

import java.util.List;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by dejv on 14/02/2017.
 */
public final class PostWeightSubscriber extends DisposableObserver<AppendValuesResponse> {

	private final IWeightDataView view;

	public PostWeightSubscriber(IWeightDataView view) {
		this.view = view;
	}

	@Override
	public void onNext(AppendValuesResponse value) {
		List<List<Object>> values = value.getUpdates().getUpdatedData().getValues();
		if (values == null) {
			view.onError(new EmptyInputException("Value cannot be empty"));
			return;
		}
		view.onComplete((String) values.get(0).get(0));

	}

	@Override
	public void onError(Throwable e) {
		view.onError(e);
	}

	@Override
	public void onComplete() {
	}
}
