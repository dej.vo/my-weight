package pl.dejv.myweight.weight.domain;

import com.google.api.services.sheets.v4.model.ValueRange;

/**
 * Created by dejv on 03/09/2017.
 */

public class InputWeightDataMapper {

	public InputWeightDataMapper() {
	}

	private String spreadSheetId;
	private String range;
	private ValueRange values;

	public String getSpreadSheetId() {
		return spreadSheetId;
	}

	public void setSpreadSheetId(String spreadSheetId) {
		this.spreadSheetId = spreadSheetId;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public ValueRange getValues() {
		return values;
	}

	public void setValues(ValueRange values) {
		this.values = values;
	}

}
